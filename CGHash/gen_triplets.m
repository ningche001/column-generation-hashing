

function triplets=gen_triplets(train_data, relevant_knn_num, irrelevant_knn_num)
    
% NOTES:
% users should implement their own code for generating triplets
% here we just show a simple example to generate triplets by multi-class label agreement.


% triplets format:
% triplets is an example index matrix of 3 columns: for each row, the first column is the target example index,
% the second column is the irrelevant (dissimilar) example index, the third column is the relevant (similar) example index.



disp('gen_triplets...');


gen_triplets_one_fn=@gen_triplets_one_multiclass;


e_num=size(train_data.feat_data, 1);
assert(e_num<2^32);

triplets=cell(e_num,1);

for e_idx=1:e_num
            
    one_triplets=gen_triplets_one_fn(train_data, e_idx, relevant_knn_num, irrelevant_knn_num);
    triplets{e_idx}=one_triplets;      
        
end

triplets=cell2mat(triplets);

end





function [one_triplets]=gen_triplets_one_multiclass(train_data, e_idx, relevant_knn_num, irrelevant_knn_num)

	label_data=train_data.label_data;
	assert(size(label_data, 1)==size(train_data.feat_data, 1));
	assert(size(label_data, 2)==1);


	one_label=label_data(e_idx);
	pos_knn_sel=label_data==one_label;
	neg_knn_sel=~pos_knn_sel;
	pos_knn_sel(e_idx)=false;


    pos_knn_inds=find(pos_knn_sel);
    neg_knn_inds=find(neg_knn_sel);

    if length(pos_knn_inds)>relevant_knn_num
    	pos_knn_inds=pos_knn_inds(randsample(length(pos_knn_inds), relevant_knn_num));
    end

    if length(neg_knn_inds)>irrelevant_knn_num
    	neg_knn_inds=neg_knn_inds(randsample(length(neg_knn_inds), irrelevant_knn_num));
    end

	relevant_knn_num=length(pos_knn_inds);
	irrelevant_knn_num=length(neg_knn_inds);   


    n_one_map_col=repmat(neg_knn_inds,relevant_knn_num, 1);
    n_one_map_col=n_one_map_col(:);
    
    p_one_map_col=repmat(pos_knn_inds',irrelevant_knn_num, 1);
    p_one_map_col=p_one_map_col(:);

    one_triplets=zeros(length(n_one_map_col),3,'uint32');
    one_triplets(:,2)=n_one_map_col;
    one_triplets(:,3)=p_one_map_col;
    one_triplets(:,1)=e_idx;
   
    one_triplets=uint32(one_triplets);
    
    
end

