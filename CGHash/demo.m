


% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au
% Correspondence should be addressed to Chunhua Shen (chhshen@gmail.com)

% Please cite this paper: 
% "Learning hash functions using column generation";
% Xi Li*, Guosheng Lin*, Chunhua Shen, Anton van den Hengel, Anthony Dick 
% (* indicates equal contribution); ICML2013;



addpath(genpath([pwd '/']));



% try larger bits: 32, 64, ...
bit_num=16;


% method running flags:

% run_CGH=false;
% run_CGH_Kernel=false;
% run_LSH=false;

run_CGH=true;
run_CGH_Kernel=true;
run_LSH=true;


%--------------------------------------------------------------------------------------------------
% load data



% try these datasets:

% ds_file='./datasets/uci_vowel.mat';
% ds_name='VOWEL';

% ds_file='./datasets/pendigits.mat';
% ds_name='PENDIGITS';

ds_file='./datasets/uci_usps.mat';
ds_name='USPS';







% load the demo dataset:
ds=load(ds_file);
ds=ds.dataset;


% here is a very simple way to split training data, just for demo

e_num=length(ds.y);
tst_e_num=min(2000, round(e_num*0.3));
tst_sel=false(e_num,1);
tst_sel(randsample(e_num,tst_e_num))=true;
ds.test_inds=find(tst_sel);

database_sel=~tst_sel;
ds.db_inds=find(database_sel);

db_e_num=length(ds.db_inds);
trn_e_num=min(2000, db_e_num);
ds.train_inds=ds.db_inds(randsample(db_e_num,trn_e_num));


database_data=[];
database_data.feat_data=ds.x(ds.db_inds,:);
database_data.label_data=ds.y(ds.db_inds);

train_data=[];
train_data.feat_data=ds.x(ds.train_inds,:);
train_data.label_data=ds.y(ds.train_inds);

test_data=[];
test_data.feat_data=ds.x(ds.test_inds,:);
test_data.label_data=ds.y(ds.test_inds);



% do normalization.

max_dims=max(train_data.feat_data,[],1);
min_dims=min(train_data.feat_data,[],1);
range_dims=max_dims-min_dims+eps;


database_data.feat_data = bsxfun(@minus, database_data.feat_data, min_dims);
database_data.feat_data = bsxfun(@rdivide, database_data.feat_data, range_dims);

train_data.feat_data = bsxfun(@minus, train_data.feat_data, min_dims);
train_data.feat_data = bsxfun(@rdivide, train_data.feat_data, range_dims);

test_data.feat_data = bsxfun(@minus, test_data.feat_data, min_dims);
test_data.feat_data = bsxfun(@rdivide, test_data.feat_data, range_dims);




% here is a simple example for generating triplets on multi-class dataset.
% user should implement their own way for generating triplets according to applications or datasets.

% generally the large value of knn_num may achieve better results, but it will be slower for large value.
relevant_knn_num=50;
irrelevant_knn_num=50;
train_data.triplets=gen_triplets(train_data, relevant_knn_num, irrelevant_knn_num);
    



%--------------------------------------------------------------------------------------------------
% run CGH

    
    % CGH configuration:
    cgh_train_info=[];

    % solver configuration for learning weights:
    cgh_train_info.tradeoff_C=10^6;
    % cgh_train_info.lbfgsb_epsilon=1e-4;


    % if running long bits or with limited memory, using stage-wise updating of weights,
    % which would be faster and use much less memory for training.
    % cgh_train_info.use_stagewise=true;


    % hash function learning configuration, 
    % usually this default setting works well.
    % cgh_train_info.wl_random_can_num=100;
    % cgh_train_info.wl_lbfgs_max_iter=50
    % cgh_train_info.wl_lbfgsb_epsilon=1e-4;



    % In the default setting, the spectral relaxation and random projection are employed as
    % initialization for hash function learning. 
    % Besides these two initialization approach, 
    % the hashing stump initialization can be turn on,       
    % however using hashing stump will make it slow, which is not recommended.
    % user need to complile the hashing stump: 
    % cd cgh/hash_stump;
    % make;
    % uncomment the following to turn on hashing stump:
    % cgh_train_info.do_wl_train_stump=true;
    

    cgh_train_info.bit_num=bit_num;
    
    
    
    
if run_CGH
    
    cgh_train_result=cgh_train(cgh_train_info, train_data);
    cgh_model=cgh_train_result.model;

    cgh_db_data_code=cgh_encode(cgh_model, database_data.feat_data);
    cgh_tst_data_code=cgh_encode(cgh_model, test_data.feat_data);


    % generate compact code if it is needed:
    % cgh_db_data_code_compact=gen_compactbit(cgh_db_data_code);
    % cgh_% user should implement their own way for generating triplets according to applications or datasets.

end


%--------------------------------------------------------------------------------------------------
% run CGH-Kernel


if run_CGH_Kernel

    cghk_train_info=cgh_train_info;

    % using RBF kernel

    % this is a simple example for picking the RBF kernel parameter sigma
    % user should tune this parameter, e.g., try to pick v from [0.1 1 5 10].

    trn_feat_data=train_data.feat_data;
    nn_k=50;
    if size(trn_feat_data,1)>1e4
        trn_feat_data=trn_feat_data(randsample(size(trn_feat_data,1), 1e4),:);
    end
    sq_eudist = sqdist(trn_feat_data',trn_feat_data');
    sq_eudist=sort(sq_eudist,2);
    sq_eudist=sq_eudist(:,1:nn_k);
    sq_sigma = mean(sq_eudist(:));
    sq_sigma=sq_sigma+eps;

    % this need to tune, try to pick v from [0.1 1 5 10]
    v=1;
    cghk_train_info.sigma=sq_sigma*v;


    % random select the support vectors, as an alternative, user can use
    % k-means for setting this support_vectors.
    sv_num=500;
    sv_num=min(sv_num, size(trn_feat_data, 1));
    support_vectors=trn_feat_data(randsample(size(trn_feat_data, 1), sv_num),:);
    cghk_train_info.support_vectors=support_vectors;


    cghk_train_result=cgh_kernel_train(cghk_train_info, train_data);
    cghk_model=cghk_train_result.model;

    cghk_db_data_code=cgh_encode(cghk_model, database_data.feat_data);
    cghk_tst_data_code=cgh_encode(cghk_model, test_data.feat_data);


end



%--------------------------------------------------------------------------------------------------
% run LSH for a simple comparison:


if run_LSH

    lsh_model=LSH_learn(train_data.feat_data, bit_num);
    lsh_db_data_code=LSH_compress(database_data.feat_data, lsh_model);
    lsh_tst_data_code=LSH_compress(test_data.feat_data, lsh_model);

end


















%--------------------------------------------------------------------------------------------------
% evaluate methods:

label_type='multiclass';

db_label_info.label_data=database_data.label_data;
db_label_info.label_type=label_type;

test_label_info.label_data=test_data.label_data;
test_label_info.label_type=label_type;

code_data_info=[];
code_data_info.db_label_info=db_label_info;
code_data_info.test_label_info=test_label_info;




%-----------------------------------------------


eva_bit_step=round(min(8, bit_num/4));
eva_bits=eva_bit_step:eva_bit_step:bit_num;


eva_param=[];
eva_param.eva_top_knn_pk=100;

predict_results=cell(0);











%-----------------------------------------------


if run_CGH
    
    cgh_eva_param=eva_param;
    
    % comment this if not using the learned weighting hamming distance,
    cgh_eva_param.use_weight_hamming=true;
    cgh_eva_param.hamming_weight=cgh_model.w;
    
    cgh_predict_result=[];
    cgh_predict_result.method_name='CGH';
    cgh_predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        cgh_code_data_info=code_data_info;
        cgh_code_data_info.db_data_code=cgh_db_data_code(:, 1:one_bit_num);
        cgh_code_data_info.tst_data_code=cgh_tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(cgh_eva_param, cgh_code_data_info);
        cgh_predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
    end
    predict_results{end+1}=cgh_predict_result;

end


if run_CGH_Kernel

    cghk_eva_param=eva_param;
    
    % comment this if not using the learned weighting hamming distance,
    cgh_eva_param.use_weight_hamming=true;
    cgh_eva_param.hamming_weight=cgh_model.w;

    cghk_predict_result=[];
    cghk_predict_result.method_name='CGH-Kernel';
    cghk_predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        cghk_code_data_info=code_data_info;
        cghk_code_data_info.db_data_code=cghk_db_data_code(:, 1:one_bit_num);
        cghk_code_data_info.tst_data_code=cghk_tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(cghk_eva_param, cghk_code_data_info);
        cghk_predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
    end
    predict_results{end+1}=cghk_predict_result;

end



%-----------------------------------------------

if run_LSH

    lsh_predict_result=[];
    lsh_predict_result.method_name='LSH';
    lsh_predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        lsh_code_data_info=code_data_info;
        lsh_code_data_info.db_data_code=lsh_db_data_code(:, 1:one_bit_num);
        lsh_code_data_info.tst_data_code=lsh_tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(eva_param, lsh_code_data_info);
        lsh_predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
    end
    predict_results{end+1}=lsh_predict_result;

end










%-----------------------------------------------


f1=figure;
line_width=2;
xy_font_size=22;
marker_size=10;
legend_font_size=15;
xy_v_font_size=15;
title_font_size=xy_font_size;

legend_strs=cell(length(predict_results), 1);

for p_idx=1:length(predict_results)
    
    predict_result=predict_results{p_idx};
   
    fprintf('\n\n-------------predict_result--------------------------------------------------------\n\n');
    disp(predict_result);

    color=gen_color(p_idx);
    marker=gen_marker(p_idx);
    
    x_values=eva_bits;
    y_values=predict_result.pk100_eva_bits;

    p=plot(x_values, y_values);
    
    set(p,'Color', color)
    set(p,'Marker',marker);
    set(p,'LineWidth',line_width);
    set(p,'MarkerSize',marker_size);
    
    legend_strs{p_idx}=[predict_result.method_name  sprintf('(%.3f)', y_values(end))];
    
    hold all
end


hleg=legend(legend_strs);
h1=xlabel('Number of bits');
h2=ylabel('Precision @ K (K=100)');
title(ds_name, 'FontSize', title_font_size);

set(gca,'XTick',x_values);
xlim([x_values(1) x_values(end)]);
set(hleg, 'FontSize',legend_font_size);
set(hleg,'Location','SouthEast');
set(h1, 'FontSize',xy_font_size);
set(h2, 'FontSize',xy_font_size);
set(gca, 'FontSize',xy_v_font_size);
set(hleg, 'FontSize',legend_font_size);
grid on;
hold off

























