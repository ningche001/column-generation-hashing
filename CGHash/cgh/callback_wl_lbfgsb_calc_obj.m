
function obj_value=callback_wl_lbfgsb_calc_obj(w, aux_data)

relation_map=aux_data{1};
relation_weights=aux_data{2};
feat_data=aux_data{3};
approx_fator=aux_data{4};
v=aux_data{5};

b=1;
if approx_fator~=1
    feat_data=approx_fator.*feat_data;
    b=approx_fator;
end


t=feat_data*w(1:end-1)' + b*w(end);

exp_m_t=exp(-t);
logistic_t=1./(1+exp_m_t);

r_H=logistic_t(relation_map(:,1))-logistic_t(relation_map(:,2));

obj_value=(r_H.^2).*relation_weights;
obj_value=-sum(obj_value)+sum(w.^2)*v;


end
