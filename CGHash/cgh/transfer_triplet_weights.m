function duplet_weights=transfer_triplet_weights(triplet_weights, wlearner_cache_duplet)

                
        triple_r_inds_duplets=wlearner_cache_duplet.triple_r_inds_duplets;

        d_num=length(triple_r_inds_duplets);
        duplet_weights=zeros(d_num,1);

        for d_idx=1:d_num
        	one_inds=triple_r_inds_duplets{d_idx};
        	if ~isempty(one_inds)
            	duplet_weights(d_idx)=sum(triplet_weights(one_inds));
        	end
        end
        
end