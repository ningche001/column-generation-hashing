



% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au





function [wlearner cache_info] =gen_wlearner(train_info, train_data, cache_info, ...
	relation_weights, transfer_cache_duplet)



relation_map=transfer_cache_duplet.relation_map_duplet;    
relevant_sel_duplet=transfer_cache_duplet.relevant_sel_duplet;



assert(~issparse(relation_weights));
assert(~issparse(relation_map));
assert(size(relation_map,2)==2);
assert(isa(relation_map,'uint32'));



zero_thresh=1e-10;
r_sel=relation_weights>zero_thresh;
relation_weights(~r_sel)=0;
valid_r_inds=find(r_sel);
valid_r_num=length(valid_r_inds);


if valid_r_num>0
    relation_weights=valid_r_num*(relation_weights./norm(relation_weights,1));
else
    wlearner=[];
    fprintf('\n\n------------------- WARNING: wlearner valid_r_num == 0 ------------------------\n\n');
    return;
end



        
    
wlearner_score=-inf;
wlearner=[];
wl_notes=[];

fprintf('\n');
    
        
    
if train_info.do_wl_train_spectral

    tmp_t=tic;

    [one_wlearner] =gen_wl_spectral(cache_info, train_data, relation_weights, ...
        relation_map, train_info, valid_r_inds, relevant_sel_duplet);
    
    one_wlearner_score=calc_wl_score(train_data.feat_data, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);

    one_wl_notes='spectral';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end

    one_wl_t=toc(tmp_t);
    fprintf('---hash_fun_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);

end





if train_info.do_wl_train_random

    tmp_t=tic;

    [one_wlearner] =gen_wlearner_random_duplet(cache_info, train_data, relation_weights,...
        relation_map, train_info, valid_r_inds, relevant_sel_duplet);
    
    one_wlearner_score=calc_wl_score(train_data.feat_data, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);

    one_wl_notes='random';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end 

    one_wl_t=toc(tmp_t);
    fprintf('---hash_fun_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);

end
       



if train_info.do_wl_train_stump

    tmp_t=tic;


	assert(~issparse(train_data.feat_data));
    assert(isa(train_data.feat_data, 'double'));
    feat_dim_num=size(train_data.feat_data, 2);

    wlearner_cache=cache_info.wlearner_cache;
    max_dim_stump=wlearner_cache.max_dim_stump;
    if wlearner_cache.max_dim_stump<feat_dim_num
        wlearner_cache.valid_dim_inds=randsample(feat_dim_num, max_dim_stump);
        cache_info.wlearner_cache=wlearner_cache;
    end

    [one_wlearner] =gen_wlearner_stump_mex_duplet(cache_info, relation_weights,...
        valid_r_inds, relation_map, relevant_sel_duplet, train_data.feat_data);    
    
    one_wlearner_score=calc_wl_score(train_data.feat_data, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);

    one_wl_notes='stump';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end

    one_wl_t=toc(tmp_t);
    fprintf('---hash_fun_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);
end

 

   
if train_info.do_wl_train_lbfgs
    
    tmp_t=tic;
    
    init_wlearner=wlearner;
    one_wlearner =gen_wlearner_lbfgs(cache_info, train_data, relation_weights, relation_map, ...
        train_info, valid_r_inds, relevant_sel_duplet, init_wlearner);
        
    
    one_wlearner_score=calc_wl_score(train_data.feat_data, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);
          
    one_wl_notes='lbfgs';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end 

    one_wl_t=toc(tmp_t);
    fprintf('---hash_fun_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);
    
end

       


    
end












function [wlearner] =gen_wlearner_random_duplet(cache_info, train_data, ...
	 relation_weights, relation_map, train_info, valid_r_inds, relevant_sel)



    can_num=100;
    if isfield(train_info, 'wl_random_can_num')
    	can_num=train_info.wl_random_can_num;
    end

    feat_data=train_data.feat_data;
    dim_num=size(feat_data,2);
    sample_mean=zeros(can_num, dim_num);
    
    w_cans=normrnd(sample_mean, 1);
    b_cans=rand(can_num,1);
    w_cans_ext=cat(2, w_cans,b_cans);
    
    wlearner_scores=calc_wl_score_wlearners(feat_data,...
        relation_map, relation_weights, relevant_sel, w_cans_ext);
    
        
    [~, sel_idx]=max(wlearner_scores);
    wlearner=w_cans_ext(sel_idx,:);
 

end












function [wlearner] =gen_wlearner_lbfgs(cache_info, train_data, relation_weights, ...
	 relation_map, train_info, valid_r_inds, relevant_sel_duplet, init_wlearner)


assert(~isempty(init_wlearner));

relation_map=relation_map(valid_r_inds,:);
relation_weights=relation_weights(valid_r_inds,:);
relevant_sel_duplet=relevant_sel_duplet(valid_r_inds,:);
relation_weights(relevant_sel_duplet)=-relation_weights(relevant_sel_duplet);

aux_data=cell(0);
aux_data{1}=relation_map;
aux_data{2}=relation_weights;
aux_data{3}=train_data.feat_data;

approx_fator=1;
aux_data{4}=approx_fator;

wlearner_reg_v=0;
aux_data{5}=wlearner_reg_v;



lbfgs_max_iter=50;
if isfield(train_info, 'wl_lbfgs_max_iter')
	lbfgs_max_iter=train_info.wl_lbfgs_max_iter;
end

lbfgsb_epsilon=1e-4;
if isfield(train_info, 'wl_lbfgsb_epsilon')
	lbfgsb_epsilon=train_info.wl_lbfgsb_epsilon;
end

wlearner =do_gen_wlearner_random_lbfgs(aux_data, init_wlearner, lbfgs_max_iter, lbfgsb_epsilon);

end



function wlearner=do_gen_wlearner_random_lbfgs(aux_data, init_wlearner, maxiter, lbfgsb_epsilon)



objFv_func_name='callback_wl_lbfgsb_calc_obj';
grad_func_name='callback_wl_lbfgsb_calc_grad';


wlearner=init_wlearner;

w_dim=length(init_wlearner);
lb=-inf(w_dim,1);
ub=inf(w_dim,1);

assert(size(init_wlearner, 1)==1);

try
    wlearner = lbfgsb(wlearner, lb, ub, objFv_func_name, grad_func_name, aux_data, [], 'factr', ...
        lbfgsb_epsilon, 'maxiter', maxiter);
catch err_info
    
    disp(err_info);
    
    fprintf('\n\n ----------------------- WARNING: wlearner lbfgs solver failed. --------------------- \n\n');
    
    % keyboard;
end

end







function [wlearner] =gen_wl_spectral(cache_info, train_data, relation_weights, ...
    relation_map, train_info, valid_r_inds, relevant_sel_duplet)

relation_map=relation_map(valid_r_inds,:);
relation_weights=relation_weights(valid_r_inds,:);
relevant_sel_duplet=relevant_sel_duplet(valid_r_inds,:);

feat_data=train_data.feat_data;


e_num=size(feat_data, 1);
weight_mat=zeros(e_num, e_num);
tmp_inds=sub2ind(size(weight_mat), relation_map(:, 1), relation_map(:, 2));
tmp_weight=-relation_weights;
tmp_weight(relevant_sel_duplet)=-tmp_weight(relevant_sel_duplet);
weight_mat(tmp_inds)=tmp_weight;
weight_mat=weight_mat+weight_mat';


RM=cache_info.wlearner_cache.RM;
LM = feat_data'*weight_mat*feat_data;
LM=LM+LM';
[U, V]=eigs(LM, RM, 1, 'la', struct([]));


wlearner = U(:,1);
tmp_v = wlearner'*RM*wlearner;
wlearner = sqrt(e_num/tmp_v)*wlearner;
wlearner=wlearner';

wlearner=cat(2, wlearner, 0);
    
end







function wlearner_score=calc_wl_score(feat_data, relation_map, relation_weights, relevant_sel_duplet, wlearner)


assert(size(wlearner, 1)==1);
assert(size(wlearner, 2)==size(feat_data, 2)+1);

hfeat=feat_data*wlearner(1:end-1)';
hfeat=hfeat+wlearner(end);

hfeat=hfeat>0;

pair_dist=hfeat(relation_map(:,1))~=hfeat(relation_map(:,2));
scores=pair_dist.*relation_weights;
scores(relevant_sel_duplet)=-scores(relevant_sel_duplet);
wlearner_score=sum(scores);


end






function wlearner_scores=calc_wl_score_wlearners(feat_data,...
    relation_map, relation_weights, relevant_sel_duplet, wlearners)


assert(size(wlearners, 2)==size(feat_data, 2)+1);
hfeat=feat_data*wlearners(:, 1:end-1)';
hfeat=hfeat+repmat(wlearners(:, end)', size(feat_data, 1), 1);

hfeat=hfeat>0;

relation_weights(relevant_sel_duplet)=-relation_weights(relevant_sel_duplet);
r1=relation_map(:,1);
r2=relation_map(:,2);

wlearner_scores=zeros(size(wlearners,1), 1);

% for memory problem, precess one by one:
for w_idx=1:size(wlearners,1)
    one_hfeat=hfeat(:,w_idx);
    pair_dist=one_hfeat(r1)~=one_hfeat(r2);
    one_score=sum(relation_weights(pair_dist));
    wlearner_scores(w_idx)=one_score;
end


end




