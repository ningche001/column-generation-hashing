
function hs_feat=apply_wlearner(feat_data, wlearners, wl_model)

if size(wlearners, 2)>size(feat_data, 2)
    feat_data=gen_feat_ext(feat_data);
end
hs_feat=feat_data*wlearners'>0;

if ~islogical(hs_feat)
    hs_feat=hs_feat>0;
end


hs_feat=full(hs_feat);

end



function feat_ext=gen_feat_ext(feat_data)

    feat_ext=cat(2, feat_data, ones(size(feat_data,1), 1));
    
end






