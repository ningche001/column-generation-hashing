function grad=callback_lbfgsb_calc_grad_l2(w, aux_data)


C_d_m=aux_data{1};
feat_data=aux_data{2};
m_rescale_vs=aux_data{3};

cur_mu_noexp=max(m_rescale_vs-feat_data*w,0);
grad=1-2*C_d_m*(cur_mu_noexp'*feat_data);


end

