
% compile hashing stump, run in MATLAB


src_file='gen_wlearner_stump_mex_duplet.cpp';


opts={'-largeArrayDims'};

% compile options including openmp support for C++ files
if( ispc ), optsOmp={'OPTIMFLAGS="$OPTIMFLAGS','/openmp"'}; else
  optsOmp={'CXXFLAGS="\$CXXFLAGS','-fopenmp"'};
  optsOmp=[optsOmp,'LDFLAGS="\$LDFLAGS','-fopenmp"'];
end

opts=[optsOmp opts];

fprintf(' -> %s\n',src_file); 
mex(src_file,opts{:});

  
  


