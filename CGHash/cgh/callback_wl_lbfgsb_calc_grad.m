function grad=callback_wl_lbfgsb_calc_grad(w, aux_data)

relation_map=aux_data{1};
relation_weights=aux_data{2};
feat_data=aux_data{3};
approx_fator=aux_data{4};

v=aux_data{5};

b=1;
if approx_fator~=1
    feat_data=approx_fator.*feat_data;
    b=approx_fator;
end



t=feat_data*w(1:end-1)' + b*w(end);


exp_m_t=exp(-t);
logistic_t=1./(1+exp_m_t);

r1=relation_map(:,1);
r2=relation_map(:,2);

r_H=logistic_t(r1)-logistic_t(r2);


tmp_grad=(1+exp_m_t).^(-2).*exp_m_t;
tmp_grad_left=tmp_grad(r1);
tmp_grad_right=tmp_grad(r2);


tmp_grad2=-2.*r_H.*relation_weights;
tmp_grad_left=tmp_grad_left.*tmp_grad2;
tmp_grad_right=tmp_grad_right.*tmp_grad2;


v1=accumarray(r1, tmp_grad_left);
v2=accumarray(r2, tmp_grad_right);

e_num=size(feat_data,1);
if length(v1)<e_num
    v1(e_num)=0;
end
if length(v2)<e_num
    v2(e_num)=0;
end


v3=v1-v2;

grad=bsxfun(@times, v3, feat_data);
grad=sum(grad,1);

tmp_b=sum(v3)*b;
grad=cat(2, grad, tmp_b);


if v>0
    grad=grad+2*v*w;
end


end
