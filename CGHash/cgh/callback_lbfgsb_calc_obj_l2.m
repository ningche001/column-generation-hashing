function [obj_value dual_sol]=callback_lbfgsb_calc_obj_l2(w, aux_data)

C_d_m=aux_data{1};
feat_data=aux_data{2};
m_rescale_vs=aux_data{3};

cur_mu_noexp=max(m_rescale_vs-feat_data*w,0);
loss_sum=sum(cur_mu_noexp.^2);


dual_sol=cur_mu_noexp;
obj_value=sum(w)+C_d_m*loss_sum;


end