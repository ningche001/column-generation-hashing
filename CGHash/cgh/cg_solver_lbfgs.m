


% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au




function train_result_sub=cg_solver_lbfgs(train_info, work_info)


% Using L-BFGS-B to solve optimization problem


%loss_type='squared_hinge';

C=train_info.tradeoff_C;

lbfgsb_epsilon=1e-4;
if isfield(train_info,'lbfgsb_epsilon')
   lbfgsb_epsilon=train_info.lbfgsb_epsilon;
end


m_rescale_vs=ones(work_info.pair_num, 1);
new_w_dim=size(work_info.new_pair_feat, 2);
w_dim=work_info.wlearner_dimension;
new_w_dims=w_dim-new_w_dim+1:w_dim;

if train_info.use_stagewise
    feat_data=work_info.new_pair_feat;
else
    feat_data=work_info.pair_feat;
end


init_w=zeros(w_dim,1);
pre_obj_value=inf;


if ~work_info.solver_init_iter
    
    pre_train_cache=work_info.train_cache_sub;
    last_w_dim=length(pre_train_cache.w);
    init_w(1:last_w_dim)=pre_train_cache.w;
    pre_obj_value=pre_train_cache.obj_value;

    w_delta_h=pre_train_cache.w_delta_h;
    sum_other_w=sum(pre_train_cache.w);

else
        
    w_delta_h=zeros(size(feat_data,1), 1);
    sum_other_w=0;
    
end

w=init_w;




C_d_m=C/work_info.pair_num;


if train_info.use_stagewise
    obj_value_func_name='callback_lbfgsb_calc_obj_l2_stage';
    grad_func_name='callback_lbfgsb_calc_grad_l2_stage';
else
    obj_value_func_name='callback_lbfgsb_calc_obj_l2';
    grad_func_name='callback_lbfgsb_calc_grad_l2';
end



aux_data{1} = C_d_m;
aux_data{2} = feat_data;
aux_data{3} = m_rescale_vs;
aux_data{4} = w_delta_h;
aux_data{5} = sum_other_w;



try
    
    if train_info.use_stagewise
        
        lb = zeros(new_w_dim,1);
        ub =  inf(new_w_dim,1);
        
        one_w=w(new_w_dims);
        one_w = lbfgsb(one_w, lb, ub, obj_value_func_name, grad_func_name, aux_data, [], 'factr', lbfgsb_epsilon);
        w(new_w_dims)=one_w;
        
    else
        
        lb = zeros(w_dim,1);
        ub =  inf(w_dim,1);
        
        w = lbfgsb(w, lb, ub, obj_value_func_name, grad_func_name, aux_data, [], 'factr', lbfgsb_epsilon);
    end
        
catch err_info

    disp(err_info);
%     dbstack; keyboard;

end
 

if train_info.use_stagewise
    one_w=w(new_w_dims);
    [obj_value dual_sol]=feval(obj_value_func_name, one_w, aux_data);
else
    [obj_value dual_sol]=feval(obj_value_func_name, w, aux_data);
end


if ~isempty(pre_obj_value)
    diff=obj_value-pre_obj_value;
    if diff>1e-8
        fprintf('\n\n ============= WARNING :obj_value increased! value: %.9f ============= \n\n', diff);
%         keyboard;
    end
end



if train_info.use_stagewise
    
    assert(size(feat_data, 2)==new_w_dim);
    w_delta_h=w_delta_h+feat_data*w(new_w_dims);

end



train_cache=[];
train_cache.w=w;
train_cache.obj_value=obj_value;
train_cache.w_delta_h=w_delta_h;


train_result_sub=[];
train_result_sub.w=w;
train_result_sub.obj_value=obj_value;
train_result_sub.wlearner_pair_weight=dual_sol;
train_result_sub.wlearner_pair_idxes=[];


work_info.train_cache_sub=train_cache;
train_result_sub.work_info=work_info;

end





