
function train_result=cgh_kernel_train(train_info, train_data)


assert(~issparse(train_data.feat_data));

[train_data.feat_data wl_kernel_model]=gen_kernel_feat(train_info, train_data.feat_data);

train_info.train_id='CGH-Kernel';

train_result=cgh_train(train_info, train_data);
train_result.model.wl_kernel_model=wl_kernel_model;

   
end
