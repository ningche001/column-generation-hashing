


function cache_info=gen_wlearner_cache(train_info, train_data, cache_info)


disp('gen_wlearner_cache...');


wlearner_cache=[];

if train_info.do_wl_train_stump
    wlearner_cache=gen_wlearner_cache_stump(train_data, train_info, wlearner_cache);
end


if train_info.do_wl_train_spectral

   feat_data=train_data.feat_data;
   RM = feat_data'*feat_data; 

   % to avoid sigular case...
   if rcond(RM) <1e-4
       tmp_num=size(RM, 1);
       RM=RM+diag(1e-4*rand(tmp_num, 1));
   end
   
   wlearner_cache.RM=RM;
end



cache_info.wlearner_cache=wlearner_cache;


end



function wlearner_cache=gen_wlearner_cache_stump(train_data, train_info, wlearner_cache)


thread_num=4;
if isfield(train_info,'thread_num')
    thread_num=train_info.thread_num;
end

if thread_num>1
    unix(['export OMP_NUM_THREADS=' num2str(thread_num) , '-echo']);
    unix('export OMP_WAIT_POLICY=PASSIVE', '-echo');
end


max_dim_stump=200;
if isfield(train_info, 'max_dim_stump')
    max_dim_stump=train_info.max_dim_stump;
end
wlearner_cache.max_dim_stump=max_dim_stump;


max_thresh_num=300;
if isfield(train_info, 'max_thresh_num')
  max_thresh_num=train_info.max_thresh_num;
end

thresh_sample_type=1;
if isfield(train_info, 'thresh_sample_type')
  thresh_sample_type=train_info.thresh_sample_type;
end




feat_data=train_data.feat_data;
assert(~issparse(feat_data));
assert(isa(feat_data, 'double'));



dim_num=size(feat_data,2);
thresholds_dims=cell(dim_num,1);


for d_idx=1:dim_num
        
    
    one_dim_feat=feat_data(:,d_idx);
    feat_vs=full(unique(one_dim_feat));
    
    one_threshs=[];
    if length(feat_vs)>=2
        one_threshs=(feat_vs(1:end-1)+feat_vs(2:end))./2;
    end
            
    one_thresh_num=length(one_threshs);
    if one_thresh_num>max_thresh_num

            if thresh_sample_type==1
                  sample_step=floor(one_thresh_num/max_thresh_num);
                  if sample_step>1
                      sel_inds=sample_step:sample_step:one_thresh_num;
                      one_threshs=one_threshs(sel_inds);
                  else
                    sel_inds=randsample(one_thresh_num, max_thresh_num);
                    sel_inds=sort(sel_inds);
                    one_threshs=one_threshs(sel_inds);
                  end
            end
            
            if thresh_sample_type==2
                sample_step=(max(one_threshs)-min(one_threshs))/max_thresh_num;
                  one_threshs=min(one_threshs):sample_step:max(one_threshs);
                  one_threshs=one_threshs';
            end
            
            if thresh_sample_type==3
                sel_inds=randsample(one_thresh_num, max_thresh_num);
                sel_inds=sort(sel_inds);
                one_threshs=one_threshs(sel_inds);
            end
          
    end
    
    thresholds_dims{d_idx}=one_threshs';
    
end



wlearner_cache.thresholds_dims=thresholds_dims;
wlearner_cache.valid_dim_inds=(1:dim_num)';


end













